// Теоретичні питання: ===================================

// Опишіть своїми словами, що таке метод об'єкту?
// --
// Метод об'єкту- це функція(ії), яка ствоюється в цьому об'єкті і має певний функціонал.

// -------------------------------------------------------
// Який тип даних може мати значення властивості об'єкта?
// --
// Тип данних об'єкта - object.
// Значення властивості об'єкта можуть бути будь-якого типу.

// -------------------------------------------------------
// Об'єкт це посилальний тип даних. Що означає це поняття?
// --
// Це означає, що об'єкт зберігається за посиланням.

// Завдання: =============================================
    
    let createNewUser = function() {       
        let userName = prompt("Enter your firstName: ");
        let userSurname = prompt("Enter your lastName: ");
        
        let newUser = {
            firstName: userName,
            lastName: userSurname,
            
            getLogin() {
                let firstLet = this.firstName[0].toLowerCase();
                let login = firstLet + this.lastName.toLowerCase();
            return login; 
            } 
        }
        return newUser;        
    }   

let createUser = createNewUser();
console.log(createUser.getLogin());
